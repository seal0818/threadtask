import sun.misc.Queue;

import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ThreadTask {
    private static final int MAX_VALUE = 1000000;//(int) Math.pow(2, 64);
    private static final int TASK_NUMBER = 15;// Runtime.getRuntime().availableProcessors() - 1;

    public static void main(String[] args) throws IOException, InterruptedException {
        while (true) {
            go();
        }
    }

    private static void go() throws FileNotFoundException, InterruptedException {
        PrimaryNumberyHolder primaryNumberyHolder = PrimaryNumberyHolder.getInstance();
        ExecutorService executor = Executors.newFixedThreadPool(TASK_NUMBER);
        CountDownLatch latch = new CountDownLatch(TASK_NUMBER);

        long startTime = System.currentTimeMillis();
        for (int i = 1; i <= TASK_NUMBER; i++) {
            new Thread(new PrimaryMiller(i, latch))
                    .start();
            //executor.execute(new Primary(i, fileWriter, latch));
        }

        latch.await();
        executor.shutdown();
        System.out.println(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime));
        System.out.println(System.currentTimeMillis() - startTime);

        primaryNumberyHolder.close();

        Scanner scanner = new Scanner(new File(PrimaryNumberyHolder.RESULT));

        int c;
        int character = scanner.nextInt();
        boolean flag = true;
        while(scanner.hasNextInt()) {
            c = scanner.nextInt();
            if (character > c) {
                flag = false;
                System.out.println(character);
                System.out.println(c);
                break;
            }
            character = c;
        }
        if (!flag)
            Thread.sleep(100000);
        primaryNumberyHolder.init();
        System.out.println(flag);
    }

    static class PrimaryMiller implements Runnable {
        private final static String fileName = "thread";
        private final static String fileExtension = ".txt";

        private final CountDownLatch latch;
        private final PrimaryNumberyHolder primaryNumberyHolder;
        private final PrintWriter ownFile;

        PrimaryMiller(int threadNumber, CountDownLatch latch) throws FileNotFoundException {
            this.primaryNumberyHolder = PrimaryNumberyHolder.getInstance();
            this.ownFile = new PrintWriter(fileName + threadNumber + fileExtension);
            this.latch = latch;
        }


        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            int counter;
            try {
                while ((counter = primaryNumberyHolder.getCounter(threadName)) < ThreadTask.MAX_VALUE) {
                    int maxValueForCurrentSequnce = counter + PrimaryNumberyHolder.STEP;
                    StringBuilder result = new StringBuilder();

                    for (int currentValue = counter; currentValue < maxValueForCurrentSequnce; currentValue++) {
                        if (!this.isPrimeSimple(currentValue))
                            continue;
                        this.ownFile.write(currentValue + " ");
                        result.append(currentValue).append(" ");
                    }

//                    for (int currentValue = counter; currentValue < maxCalueForCurrentSequnce; currentValue++) {
//                        if (!BigInteger.valueOf(currentValue).isProbablePrime((int) Math.log(currentValue)))
//                            continue;
//                        this.ownFile.write(currentValue + " ");
//                        result.append(currentValue).append(" ");
//                    }
                    this.primaryNumberyHolder.writeIntoFile(result.toString());
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            latch.countDown();
            this.closeResources();
        }

        private boolean isPrimeSimple(int counter) {
            boolean isSimple = true;
            if (counter % 2 == 0 && counter != 2)
                return false;
            for (int j = 3; j < Math.sqrt(counter); j += 2) {
                if (counter % j == 0) {
                    isSimple = false;
                    break;
                }
            }
            return isSimple;
        }

        private void closeResources() {
            this.ownFile.close();
        }
    }


    static class PrimaryNumberyHolder {

        private static final int STEP = 100;

        private static final String RESULT = "result.txt";

        static class ValueHolder implements Comparable<ValueHolder> {
            private int currentValue;
            private boolean isMin;

            ValueHolder(int currentValue, boolean isMin) {
                this.currentValue = currentValue;
                this.isMin = isMin;
            }

            public boolean isMin() {
                return isMin;
            }

            public void setMin(boolean min) {
                isMin = min;
            }

            public int getCurrentValue() {
                return currentValue;
            }

            public void setCurrentValue(int currentValue) {
                this.currentValue = currentValue;
            }

            @Override
            public int compareTo(ValueHolder that) {
                if (Objects.isNull(that)) {
                    return 1;
                }
                return Integer.compare(this.getCurrentValue(), that.getCurrentValue());
            }

            @Override
            public boolean equals(Object o) {

                return true;
            }
        }

        private static volatile PrimaryNumberyHolder fileWriter;

        private final AtomicInteger counter = new AtomicInteger(2);

        private final Map<String, ValueHolder> holderMap = Collections.synchronizedMap(new HashMap<>());

//        private PriorityQueue<ValueHolder> holderQueue = new PriorityQueue<>(ThreadTask.TASK_NUMBER);

        private final PrintWriter result = new PrintWriter(RESULT);

        private final Semaphore semaphore = new Semaphore(1);

        private PrimaryNumberyHolder() throws FileNotFoundException {
        }

        public static PrimaryNumberyHolder getInstance() throws FileNotFoundException {
            PrimaryNumberyHolder writer = fileWriter;
            if (writer == null) {
                synchronized (PrimaryNumberyHolder.class) {
                    writer = fileWriter;
                    if (writer == null) {
                        fileWriter = writer = new PrimaryNumberyHolder();
                    }
                }
            }
            return writer;
        }

        public PrimaryNumberyHolder init() throws FileNotFoundException {
            this.fileWriter = new PrimaryNumberyHolder();
            return fileWriter;
        }

        public int getCounter(String threadName) throws InterruptedException {
            this.semaphore.acquire();
            int value = counter.getAndAdd(STEP);
            this.holderMap.put(threadName, new ValueHolder(value, false));
            this.setMinAtHolder();
//            this.holderQueue = this.holderQueue.stream().sorted(new ValueHolderComparator())
//                    .collect(Collectors.toCollection(PriorityQueue::new));
            this.semaphore.release();
            return value;
        }


        public void writeIntoFile(String value) throws InterruptedException {
            while (!this.holderMap.get(Thread.currentThread().getName()).isMin()) {
            }
            synchronized (this.result) {
                this.setMinAtHolder();
//            while (true) {
//                    if (this.holderQueue.peek().getThreadName().equals(Thread.currentThread().getName())) {
//                        break;
//                    }
//            }
//            this.holderQueue.poll();
                this.result.print(value);
            }
        }

        public void close() {
            this.result.close();
        }


        private void setMinAtHolder () {
            synchronized (this.holderMap) {
                this.holderMap.values().stream()
                        .min(Comparator.comparing(ValueHolder::getCurrentValue))
                        .get()
                        .setMin(true);            }
        }
    }
}
